FROM alt:latest

RUN apt-get update && apt-get install -y \
    gcc gcc-c++  \
    openssh openssh-server openssh-common openssh-clients openssh-keysign openssh-server-control \
    cmake rpm-build gdb gdbserver rsync vim-console
RUN apt-get install -y libomniORB libomniORB-names libuniset2 libuniset2-devel libuniset2-extension-common-devel

# SSH login fix. Otherwise user is kicked off after login
WORKDIR /
COPY ./init.sh /
RUN chmod 'a+x' ./init.sh
RUN echo "PubkeyAuthentication yes" >> /etc/openssh/sshd_config
RUN echo "PubkeyAcceptedKeyTypes ssh-dss" >> /etc/openssh/sshd_config

ADD ./src /debug
RUN useradd -ms /bin/bash debugger
RUN echo 'debugger:pwd' | chpasswd

EXPOSE 22 7777

ENTRYPOINT /init.sh; /bin/bash